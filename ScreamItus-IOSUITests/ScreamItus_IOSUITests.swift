//
//  ScreamItus_IOSUITests.swift
//  ScreamItus-IOSUITests
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import XCTest

//@testable import ScreamItus_IOS

class ScreamItus_IOSUITests: XCTestCase {
    // create a global variable
//    var infection: Infection!
    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
        
        super.setUp()
//        infection = Infection()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // QUESTION 2 - TESTS
    // TC1: When the app loads, the text box and button are visible, but the
    // results label is hidden
    func testElementsVisibility() {
        // 1.  Find the elements and check if each are visible
        // 1a. Text box
        let app = XCUIApplication()
        let textBox = app.textFields
        XCTAssertNotNil(textBox)
        
        // 1b. Button
        let button = app.buttons["Calculate"]
        XCTAssertNotNil(button)
    
        // 1c. Results label
        let label = app.staticTexts["1000 instructors infected!"]
        XCTAssertEqual(false, label.exists)
    }
    
    // TC2: When user enters a value in the text box and presses CALCULATE, the app
    // displays the correct number of infected instructors
    
    func testNumberOfInfected() {
        // 1.  Find the elements and do the actions
        // 1a. Text box
        let app = XCUIApplication()
        let textBox = app.textFields
        textBox.element.typeText("5")
        // 1b. Button
        let button = app.buttons["Calculate"]
        button.tap()

        // 1c. Results label
        let label = app.textViews
        let lblText =  label.element.value as! String
//        let numberOfInfected = infection.calculateTotalInfected(day: 5)
//        let expected = "\(numberOfInfected) instructors infected!"
//        XCTAssertEqual(expected, lblText)
    }

}
