//
//  ScreamItus_IOSTests.swift
//  ScreamItus-IOSTests
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import XCTest

@testable import ScreamItus_IOS

class ScreamItus_IOSTests: XCTestCase {
   // create a global variable
    var infection: Infection!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        infection = Infection()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //R1: The number of days must be > 0
    // TC1:Testing the number of days
    func testNumberOfDaysGreaterZero() {
        let actual = infection.calculateTotalInfected(day: 0)
        XCTAssertEqual(-1, actual)
    }


    //R2: The virus infects instructors at a rate of 5 instructors per day
    // TC2:Testing the rate of infection
    func testRateOfInfectionFive() {
        let actual = infection.calculateTotalInfected(day: 1)
        XCTAssertEqual(5, actual)
    }
    
    //R3: After 7 days, the infection rate changes to 8 instructors per day
    // TC3:Testing the rate of infection
    func testRateOfInfectionEight() {
        let actual = infection.calculateTotalInfected(day: 8)
        XCTAssertEqual(43, actual)
    }
    
    //R4: Pritesh Patel, Mohammad Kiani and Albert Danison are at the College
    // TC4:Testing the rate of infection on even days
    func testRateOfInfectionEven() {
        let actual = infection.calculateTotalInfected(day: 8)
        XCTAssertEqual(0, actual)
    }

}
